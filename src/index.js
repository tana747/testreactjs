import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
// import './elementTransitions.min.js';
import './elementTransitions.min.css';


ReactDOM.render(
  <App />,
  document.getElementById('root')
);
