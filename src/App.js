import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import './fonts.css';


// import VIewTemplete from './Components/ViewTemplete';
// import ViewPage from './Components/ViewPage';


class App extends Component {
    render() {
        return (

            <ViewTemplete/>


            );
    }
}


class ViewTemplete extends Component {

    render() {
        return (
            <div className="flex-boxes" id="myDiv">            
             <ViewPage/>
         </div>
            );
    }
}

var stations = {
    css: ".elem1 {padding: 10px 16px;font-size: 18px;line-height: 1.33;border-radius: 6px;}",
    element: [{
        type: 'text',
        data: ""
    }
    ]
};


var page1 = {
    css: ".elem0 img {display: block;width: 100%;margin: 0 auto 1em;opacity: 1; }.elem1 { font-family: 'Conv_Pattaya-Regular', Sans-Serif; top: 82%;  position: absolute;  z-index: 1000;  color: #FFF;  width: 87%;  font-size: 72px;  transform: rotate(-10deg); }",
    element: [{
        type: 'img',
        data: "http://localhost/testsass/img/template1_01.png"
    },
    {
        type: 'text',
        data: "Good music Good life."
    }
    ]
};



var page2 = {
    "css": ".elem2 img {  display: block;  width: 100%;  margin: 0 auto 1em;  opacity: 1; }  .elem3 {  font-family: 'Conv_Pattaya-Regular', Sans-Serif;  top: 82%;  position: absolute;  z-index: 1000;  color: #FFF;  width: 87%;  font-size: 72px;  transform: rotate(-10deg); }",
    "element": [{
        "type": "img",
        "data": "http://localhost/testsass/img/template1_02.png"
    },
    {
        "type": "text",
        "data": "Chapter 1"
    }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
};

var page3 = {
    "css": ".elem4 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  background-color: #000;  color: #FFF;  text-align: left;  font-size: 30px;  padding-top: 30px;  padding-left: 5%; }  .elem4 p {    margin: 0px 0 0px 0; }   .elem5 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  background-color: #000;  color: #ccc;  text-align: left;  font-size: 23px;  padding-left: 5%; }  .elem5 p {    margin: 0px 0 0px 0; } .elem6 img {  display: block;  width: 100%;  margin: 0 auto 1em;  opacity: 1; }.elem6 video {  display: block;  width: 100%;  margin: 0 auto 1em;  opacity: 1; }  .elem7 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 45%;  height: 50px;  font-size: 21px; } .elem8 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 45%;  float: left; } .elem9 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 45%;  float: right;  top: -50px;  position: relative; } ",
    "element": [
    {
        "type": "text",
        "data": "Good music Good life."
    },
    {
        "type": "text",
        "data": "ดนตรีเป็นสิ่งที่ธรรมชาติให้มาพร้อม ๆ กับชีวิตมนุษย์โดยที่มนุษย์เองไม่รู้ตัว..."
    },
    {
        "type": "img",
        "data": "http://localhost/testsass/img/template1_03_02.png"
    },
    {
        "type": "text",
        "data": "Good music Good life."
    },
    {
        "type": "text",
        "data": "มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
    },
    {
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน"
    }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
};


var page4 = {
    "css": " .elem10 {  width: 20%;  background-color: #000;  height: 100%;  float: left; }  .elem11 {  width: 79%;  float: left; }  .elem11 img {    display: block;    width: 100%;    margin: 0 auto 1em;    opacity: 1; }  .elem12 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 79%;  float: left;  padding-left: 35px;  text-align: left; }   ",
    "element": [{
        "type": "text",
        "data": " "
    },
    {
        "type": "img",
        "data": "http://localhost/testsass/img/04_02.png"
    },
    {
        "type": "text",
        "data": "ดนตรีเป็นศิลปะที่อาศัยเสียงเพื่อเป็นสื่อในการถ่าย ทอดอารมณ์ความรู้สึกต่าง ๆไปสู่ผู้ฟังเป็นศิลปะที่ง่ายต่อ การสัมผัส ก่อให้เกิดความสุขความปลื้มปิติพึงพอใจให้ แก่มนุษย์ได้ นอกจากนี้ได้มีนักปราชญ์ท่านหนึ่งได้กล่าว ไว้ว่า “ดนตรีเป็นภาษาสากลของมนุษยชาติเกิดขึ้นจาก ธรรมชาติและมนุษย์ได้นำมาดัดแปลงแก้ไขให้ประณีต งดงามไพเราะเมื่อฟังดนตรีแล้วทำให้เกิดความรู้สึกนึกคิดต่างๆ” นั้นก็เป็นเหตุผลหนึ่งที่ทำให้เราได้ทราบว่ามนุษย์ ไม่ว่าจะเป็นชนชาติใดภาษาใดก็สามารถรับรู้อรรถรสของดนตรีได้โดยใช้เสียงเป็นสื่อได้เหมือนกัน มีบุคคลจำนวนไม่น้อยที่ตั้งคำถามว่า “ดนตรีคืออะไร” แล้ว “ทำไมต้องมีดนตรี” คำว่า “ดนตรี” ในพจนานุกรม ฉบับราชบัณฑิตยสถาน พ.ศ. 2525 ได้ให้ความหมายไว้ ว่า “เสียงที่ประกอบกันเป็นทำนองเพลง เครื่องบรรเลง ซึ่งมีเสียงดังทำให้รู้สึกเพลิดเพลิน หรือเกิดอารมณ์รัก โศก หรือรื่นเริง”จากความหมายข้างต้นจึงทำให้เราได้ทราบ คำตอบที่ว่าทำไมต้องมีดนตรีก็เพราะว่าดนตรีช่วยทำให้ มนุษย์เรารู้สึกเพลิดเพลินได้"
    }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
};


var page5 = {
    "css": ".elem13 {  width: 25%;  float: right; }  .elem13 img {    display: block;    width: 100%;    opacity: 1; }.elem13:parent {  background: #F00; } .elem14 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 73%;  height: 40%;  float: right;  background-color: #000;  color: #FFF;  position: relative; }  .elem14 p {    width: 50%;    /* height: 100%; */    float: right;    font-size: 30px;    vertical-align: 80%;    text-align: right;    padding-right: 20px;    position: absolute;    bottom: 0px;    right: 0px; } .elem15 {  font-family: 'Conv_CSChatThaiUI', Sans-Serif;  width: 73%;  float: right;  background-color: #000;  color: #FFF;  height: 60%; }  .elem15 p {    width: 80%;    height: 100%;    float: right;    text-align: right;    padding-right: 20px; }",
    "element": [{
        "type": "img",
        "data": "http://localhost/testsass/img/template1_05_02.png"
    },
    {
        "type": "text",
        "data": "a melody also tune."
    },
    {
        "type": "text",
        "data": "“ดนตรี” มีความหมาย ที่กว้างและหลากหลายมาก การนำดนตรี ไปใช้ประกอบต่างๆที่เราคุ้นเคยเช่น การใช้ประกอบในภาพยนต์ เนื่องจาก ดนตรีนั้นสามารถนำไปเป็นพื้นฐานใน การสร้างอารมณ์ลักษณะต่างๆของ แต่ละฉากได้ พิธีกรรมทางศาสนา ก็มีการนำดนตรีเข้าไปมี ส่วนร่วมด้วย จึงทำให้มีความขลัง ความน่าเชื่อถือ ความศรัทธามีประสิทธิภาพมากขึ้น นอกจากนี้ดนตรีบางประเภทถูกนำ ไปใช้ในการเผยแพร่ความเป็นอันหนึ่ง อันเดียวกันของกลุ่มคนหรือเชื้อชาติ"
    }
    ],
    "postId": "581b11b0c67f4b5005bfcbcd",
    "hidden": "false"
};


var accessToken = '2ux8fv8Sk9cIaMwrQNVDtkvCmxBBSc0mIXRXK3pzfRyIkDvnosFK1vOKieOwVOKm';
var pageId = '5811a9e47371f484110277f6';

 var pageId = '581b15501fdd29b41158b6dc';
// var pageId = '581b15b51fdd29b41158b6dd';
// var pageId = '581b16711fdd29b41158b6de';
// var pageId = '581b16821fdd29b41158b6df';
// var pageId = '581b168b1fdd29b41158b6e0';

// const styles = StyleSheet.create(stations.css);

class ViewPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stations: stations.element,
            css: stations.css,
            index: 0
        };
        
        // console.log('state=>', stations.element);

    }
    action(data) {

          // console.log(data.index);
          this.setState({
              index: data.index
          });
      
      if(typeof data.stations !== 'undefined')
      {

          this.setState({
              stations: data.stations
          });

        fetch('http://localhost:3000/api/Pages/updatePage?access_token=' + accessToken,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    pageId: pageId,
                    element: this.state.stations
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                // console.log(json.element)
              }).catch(function(ex) {
            console.log('parsing failed', ex)
        });


      }
      // console.log(data.stations);
    }


    componentDidMount(){
       // console.log('fetch');       
        fetch('http://localhost:3000/api/Pages/detail?access_token=' + accessToken,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    pageId: pageId
                })
            })
            .then((response) => {
                return response.json()
            })
            .then((json) => {
                // console.log(json.element, json.css)
                this.setState({
                    stations: json.element,
                    css: json.css
                });
            }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
        

    }
   
    render() {

      // console.log('render');
        return (
           
            <div className="page flex-box flex-box-big" >
            <style>    
              {this.state.css}
            </style>

            <Modals modalName="Edit" action={(data) => this.action(data)} data={this.state.stations} index={this.state.index}/>
              {console.log(this.state.stations[0].data)}
              <meta id="meta-description" name="description" content={typeof this.state.stations[2]=== 'undefined'?"":this.state.stations[2].data} />
              <meta id="og-title" property="og:title" content={typeof this.state.stations[1]!== 'undefined'?this.state.stations[1].data:""} />
              <meta id="og-image" property="og:image" content={typeof this.state.stations[0]!== 'undefined'?this.state.stations[0].data:""} />

             {this.state.stations.map(function(item, i) {
                return (
                    <Elementu type={item.type} data={item.data} e1={i} action={(data) => this.action(data)} />
                    );
            }, this)}
          </div>
        );
    }
}

class Element extends Component {
    render() {
        return (
            <div className={'elem'+this.props.e1 + " edit"}> 
            {(() => {
                switch (this.props.type) {
                case "img":
                    return <img src={this.props.data} alt="img" />;
                case "text":
                    return <p>{this.props.data}</p>;
                default:
                    return <p></p>;
                }
            })()}
        </div>
            );
    }
}


class Elementu extends Component {

    componentDidUpdate(data) {
      // console.log(this.props.data); 
    }

    render() {
        return (
            <div className={'elem'+this.props.e1 + " edit"}>           
              <a className="btn-edit" href="#Edit" onClick={() => {
                this.props.action({
                    index: this.props.e1
                });
            }}>      
             {(() => {
                switch (this.props.type) {
                case "img":
                    return <img src={this.props.data} alt="img" />;
                case "text":
                    return <p>{this.props.data}</p>;
                default:
                    return <p></p>;
                }
            })()}
             </a>            
        </div>
            );
    }
}


class Modals extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: '',
            type: '',
            e1: '',
            update: true
        };
    }

    handleField(fieldName, event) {

        let newstations = this.props.data;
        newstations[this.props.index].data=event.target.value;
        this.props.action({
            index: this.props.index,
            stations: newstations
        });
        // console.log('onChange=>',newstations);
    }


    render() {
        return (
            <div className="modal" id={this.props.modalName} aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-header">
            <h2>
              Edit
            </h2>
            <a href="#close" className="btn-close" aria-hidden="true">×</a>
          </div>
          <div className="modal-body">
            <p><label>text</label>
                <input type="text" name="data" value={this.props.data[this.props.index].data} onChange={(event) => this.handleField('data', event)}/>
            </p>
            <a href="#close" className="btn" onClick={() => this.props.action('updatePage', {
                data: this.state.data,
                type: this.state.type,
                e1: this.state.e1
            })
            }>close</a>
           
          </div>
        </div>
      </div>
            );
    }
}

export default App;
